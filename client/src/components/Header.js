import React, {Component} from 'react';
import {connect} from 'react-redux'
import {Link} from 'react-router-dom'
import { logout } from '../actions'

class Header extends Component {

  handleClick(){
    this.props.logout();
    this.props.history.push('/');
  }

  renderNav() {
    if (this.props.user) {
      return (
        <ul className="dib">
          <li className="dib mr4">
            <Link to="/obavestenja" className="link dim blue">Obavestenja</Link>
          </li>
          <li className="dib mr4">
            <button onClick={this.handleClick.bind(this)} className="link dim blue">Odjavi se</button>
          </li>
        </ul>
      );
    } else {
      return (
        <ul className="dib">
          <li className="dib mr4">
            <Link to="/login" className="link dim blue">Prijavi se</Link>
          </li>
        </ul>
      );
    }
  }

  render() {
    return (
      <div className="flex justify-between">
        <Link to="/" className="link dim blue"><h3 className="dib ml4">KUCNI SAVET</h3></Link>
        { this.renderNav() }
      </div>
    );
  }
}

function mapStateToProps({user}) {
  return {user}
}

export default connect(mapStateToProps, { logout })(Header)