import React, { Component } from 'react';
import {connect} from 'react-redux';
import NewNotification from './NewNotification'

class Dashboard extends Component {

  render(){

    return (
      <div>
        <h1 className="tc">Obavestenjaaaaa</h1>
        <NewNotification />
        { this.props.notifications.map((n, i) => {
          return (
            <div style={{ border: '1px solid gray', padding: '10px', margin: '20px 0' }} key={i}>
              <h3>{ n.title }</h3>
              <p>{ n.text }</p>
            </div>
          );
        })}
      </div>
    );

  }
}


function mapStateToProps({notifications}) {
  return {notifications}
}

export default connect(mapStateToProps)(Dashboard)
