import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form'
import {connect} from 'react-redux';
import {login} from '../actions'
import validateEmails from '../utils/validateEmails'
import Input from './Input'

const inputs = [
  {name: 'email', message: 'Morate da upisete vasu email adresu.'},
  {name: 'password', message: 'Morate da upisete vasu sifru.'},
];


class Login extends Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this)
  }

  onSubmit(values) {
    this.props.login(values, () => this.props.history.push('/obavestenja'))
  }

  render() {

    return (
      <div>
        <form onSubmit={this.props.handleSubmit(this.onSubmit)} noValidate>
          <Field name="email" type="email" label="Email" component={Input}/>
          <Field name="password" type="password" label="Sifra" component={Input}/>
          <button type="submit">Prijavi se</button>
        </form>
      </div>
    );
  }
}

function validate(values) {
  const errors = {};

  inputs.map(input => {
    if (!values[input.name]) {
      errors[input.name] = input.message
    }
    return []
  });

  errors.email = validateEmails(values.email || '');

  return errors
}

export default reduxForm({
  validate,
  form: 'loginForm'
})(
  connect(null, {login})(Login)
)