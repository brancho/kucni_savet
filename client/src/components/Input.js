import React from 'react';

const Input = ({ label, input, type, meta: { error, touched } }) => {

  return (
    <div>
      <label>{label}</label>
      <input type={ type || 'text' } {...input} />
      {touched && <small>{error}</small>}
    </div>
  );
};

export default Input






