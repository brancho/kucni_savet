import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form'
import {connect} from 'react-redux';
import {register} from '../actions'
import validateEmails from '../utils/validateEmails'
import Input from './Input'

const inputs = [
  {name: 'full_name', message: 'Morate da upisete ime.'},
  {name: 'apt_number', message: 'Morate da upisete broj stana.'},
  {name: 'email', message: 'Morate da upisete vasu email adresu.'},
  {name: 'password', message: 'Morate da upisete vasu sifru.'},
  {name: 'password_confirm', message: 'Morate da potvrdite vasu sifru.'}
];


class Register extends Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this)
  }

  onSubmit(values) {
    values.building_id = this.props.match.params.building_id;
    this.props.register(values, () => this.props.history.push('/obavestenja'))
  }

  render() {

    return (
      <div>
        <form onSubmit={this.props.handleSubmit(this.onSubmit)} noValidate>
          <Field name="full_name" label="Ime i prezime" component={Input}/>
          <Field name="email" type="email" label="Email" component={Input}/>
          <Field name="apt_number" type="text" label="Broj stana" component={Input}/>
          <Field name="password" type="password" label="Sifra" component={Input}/>
          <Field name="password_confirm" type="password" label="Potvrdi sifru" component={Input}/>
          <button type="submit">Registruj se</button>
        </form>
      </div>
    );
  }
}

function validate(values) {
  const errors = {};

  inputs.map(input => {
    if (!values[input.name]) {
      errors[input.name] = input.message
    }
    return []
  });

  if (values.password_confirm !== values.password && !!values.password_confirm) {
    errors.password_confirm = "Upisane sifre se ne poklapaju."
  }

  errors.email = validateEmails(values.email || '');

  return errors
}

export default reduxForm({
  validate,
  form: 'registerForm'
})(
  connect(null, {register})(Register)
)