import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form'
import {connect} from 'react-redux';
import { createNotification } from '../actions'
import Input from './Input'

const inputs = [
  {name: 'title', message: 'Upisite naslov vaseg obavestenja.'},
  {name: 'text', message: 'Upisite tekst vaseg obavestenja'},
];


class NewNotification extends Component {
  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this)
  }

  onSubmit(values) {
    values.building_id = this.props.user.building_id;
    this.props.createNotification(values)
  }

  render() {

    return (
      <div>
        <form onSubmit={this.props.handleSubmit(this.onSubmit)} noValidate>
          <Field name="title" type="text" label="Naslov obavestenja" component={Input}/>
          <Field name="text" type="text" label="Tekst obavestenja" component={Input}/>
          <button type="submit">Kreiraj obavestenje</button>
        </form>
      </div>
    );
  }
}

function validate(values) {
  const errors = {};

  inputs.map(input => {
    if (!values[input.name]) {
      errors[input.name] = input.message
    }
    return []
  });

  return errors
}

function mapStateToProps({user}) {
  return {user}
}

export default reduxForm({
  validate,
  form: 'NotificationForm'
})(
  connect(mapStateToProps, {createNotification})(NewNotification)
)