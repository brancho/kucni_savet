import {createStore, applyMiddleware} from 'redux';
import reduxPromise from 'redux-promise'
import reduxThunk from 'redux-thunk'
import rootReducer from './reducers';
import { loadState, saveState } from './localStorage'

const configureStore = () => {
  const persistedState = loadState();
  const store = createStore(
    rootReducer,
    persistedState,
    applyMiddleware(reduxPromise, reduxThunk)
  );
  store.subscribe(() => {
    saveState(store.getState())
  });
  return store;
};

export default configureStore