import { SET_CURRENT_USER, REMOVE_CURRENT_USER, ADD_NOTIFICATION, GET_NOTIFICATIONS } from '../actions/types'
import axios from 'axios'

export const register = (user, redirect) => async dispatch => {

  const response = await axios.post('/api/register', user);
  const notif = await axios.get('/api/notification');

  const { full_name, email } = response.data;
  redirect();
  dispatch({ type: SET_CURRENT_USER, payload: { full_name, email, building_id: user.building_id } });
  dispatch({ type: GET_NOTIFICATIONS, payload: notif.data });
};

export const login = (user, redirect) => async dispatch => {

  const userr = await axios.post('/api/login', user);
   const notif = await axios.get('/api/notification');
  redirect();
  dispatch({ type: SET_CURRENT_USER, payload: { ...userr.data } });
   dispatch({ type: GET_NOTIFICATIONS, payload: notif.data });
};

export const logout = () => async dispatch => {
  await axios.get('/api/logout');
  dispatch({ type: REMOVE_CURRENT_USER });
};

export const createNotification = (notification) => async dispatch => {
  const response = await axios.post('/api/notification', notification);
  const { text, title } = response.data

  dispatch({ type: ADD_NOTIFICATION, payload: {text, title} })
};

