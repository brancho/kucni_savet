import { ADD_NOTIFICATION, GET_NOTIFICATIONS } from '../actions/types'

export default function(state = [], action){
  switch(action.type){
    case GET_NOTIFICATIONS:
      return action.payload;
    case ADD_NOTIFICATION:
      return [ action.payload, ...state ];
    default:
      return state
  }
}





