import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form'
import currentUserReducer from './currentUserReducer'
import notificationsReducer from './notificationsReducer'


const rootReducer = combineReducers({
  form: formReducer,
  user: currentUserReducer,
  notifications: notificationsReducer
});

export default rootReducer;