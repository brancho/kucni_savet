import React, { Component } from 'react';
import configureStore from './configureStore'
import {Provider} from 'react-redux';
import {BrowserRouter, Route} from 'react-router-dom'

/*  import components  */
import Landing from './components/Landing'
import Header from './components/Header'
import Register from './components/Register'
import Login from './components/Login'
import Dashboard from './components/Dashboard'



const store = configureStore();

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <div>
              <Route path="/" component={Header}/>
              <Route path="/login" component={Login}/>
              <Route path="/obavestenja" component={Dashboard}/>
              <Route path="/zgrade/:building_id/registracija" component={Register}/>
              <Route exact path="/" component={Landing}/>
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;

