const express = require('express');

require('../models/Resident');
require('../models/Apartament');
require('../models/Building');
require('../models/Notification');

const router = express.Router();
const userController = require('../controllers/userController');
const notificationController = require('../controllers/notificationController');

router.post('/login',
  userController.authentication,
  userController.findBuilding,
  userController.login
);
router.get('/logout', userController.logout);
router.post('/register',
  userController.validateRegister,
  userController.register,
  userController.authentication,
  userController.login
);
router.post('/notification',
  notificationController.createNotification,
);

router.get('/notification',
  userController.findBuilding,
  notificationController.getNotifications
);

module.exports = router;