const mongoose = require('mongoose');
const Resident = mongoose.model('Resident');
const passport = require('passport');

passport.use(Resident.createStrategy());

passport.serializeUser(Resident.serializeUser());
passport.deserializeUser(Resident.deserializeUser());