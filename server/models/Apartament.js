const mongoose = require('mongoose');
const { Schema } = mongoose;

const apartamentSchema = new Schema({
  apt_number: {
    type: Number,
    required: 'Upisite broj vaseg stana.',
    unique: true
  },
  building_id: { type: mongoose.Schema.ObjectId, ref: 'Building' },
  resident_ids: [ {type: mongoose.Schema.ObjectId, ref: 'Resident'} ]
});

module.exports = mongoose.model('Apartament', apartamentSchema);