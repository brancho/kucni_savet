const mongoose = require('mongoose');
const { Schema } = mongoose;

const buildingSchema = new Schema({
  address: {
    type: String,
    unique: true
  }
});

module.exports = mongoose.model('Building', buildingSchema);