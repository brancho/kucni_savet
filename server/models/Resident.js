const mongoose = require('mongoose');
const { Schema } = mongoose;
const validator = require('validator');
const passportLocalMongoose = require('passport-local-mongoose');

const residentSchema = new Schema({
  full_name: {
    type: String,
    required: 'Upisite vase ime.'
  },
  email: {
    type: String,
    unique: true,
    lowercase: true,
    trim: true,
    validate: [validator.isEmail, 'Invalid Email Address'],
    required: 'Upisite vasu email adresu.'
  },
  president: {
    type: Boolean
  }
});

residentSchema.plugin(passportLocalMongoose, {usernameField: 'email'});

module.exports = mongoose.model('Resident', residentSchema);