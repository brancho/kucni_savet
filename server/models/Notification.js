const mongoose = require('mongoose');
const { Schema } = mongoose;

const notificationSchema = new Schema({
  title: {
    type: String,
    required: 'Upisite naslov vaseg obavestenja.'
  },
  text: {
    type: String,
    required: 'Upisite tekst vaseg obavestenja'
  },
  building_id: { type: mongoose.Schema.ObjectId, ref: 'Building' },
  resident_id: { type: mongoose.Schema.ObjectId, ref: 'Resident' }
});

module.exports = mongoose.model('Notification', notificationSchema);