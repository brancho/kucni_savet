const mongoose = require('mongoose');
const Resident = mongoose.model('Resident');
const Building = mongoose.model('Building');
const Apartament = mongoose.model('Apartament');
const promisify = require('es6-promisify');
const passport = require('passport');


exports.validateRegister = (req, res, next) => {
  req.sanitizeBody('full_name');
  req.checkBody('full_name', 'Morate da upisete ime.!').notEmpty();
  req.sanitizeBody('apt_number');
  req.checkBody('apt_number', 'Morate da upisete broj stana.').notEmpty();
  req.checkBody('email', 'Email adresa nije ispravna').isEmail();
  req.sanitizeBody('email').normalizeEmail({
    remove_dots: false,
    remove_extension: false,
    gmail_remove_subaddress: false
  });
  req.checkBody('password', 'Morate da upisete vasu sifru.').notEmpty();
  req.checkBody('password_confirm', 'Morate da potvrdite sifru.').notEmpty();
  req.checkBody('password_confirm', 'Sifre se ne poklapaju.').equals(req.body.password);

  const errors = req.validationErrors();
  if (errors) {
    console.log(errors);
    return;
  }
  next();
};

exports.register = async (req, res, next) => {

  const { full_name, email, apt_number, building_id } = req.body;
  const buildingEmpty = await Apartament.findOne({ building_id });

  const resident = new Resident({
    full_name,
    email,
    president: !buildingEmpty
  });

  const register = promisify(Resident.register, Resident);
  const newUser = await register(resident, req.body.password);

  await Apartament.updateOne(
    { building_id, apt_number },
    { apt_number, building_id, $push: { resident_ids: newUser._id  }},
    { upsert : true }
  );

  next()
};


exports.authentication = passport.authenticate('local', { failureRedirect: '/oooopss' });

exports.logout = (req, res) => {
  req.logout();
  res.json({ message: `Successfully logged out` });
};


exports.findBuilding = async (req, res, next) => {
  const apartament = await Apartament.findOne({ resident_ids: req.user._id });
  req.user.building_id = apartament.building_id;
  next();
};

exports.login = (req, res) => {
  const { full_name, email, building_id } = req.user;
  res.json({ full_name, email, building_id });
};

