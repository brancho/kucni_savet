const mongoose = require('mongoose');
const Notification = mongoose.model('Notification');

exports.createNotification = async (req, res) => {

  const { title, text, building_id } = req.body;

  const resident_id = req.user._id;

  const notification = new Notification({
    title,
    text,
    building_id,
    resident_id
  });

  const newNotification = await notification.save();

  res.json(newNotification);
};

exports.getNotifications = async (req, res) => {
  const { building_id } = req.user;
  const notifications = await Notification.find({ building_id });
  res.json(notifications);
};