const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const session = require('express-session')
const passport = require('passport');
const keys = require('./server/config/keys');
const expressValidator = require('express-validator');
const s = require('./server');
require('./server/services/passport');

mongoose.connect(keys.mongoURI);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(expressValidator());
app.use(session({ secret: '123456sdfghjkd5r6tv7byin', resave: true,
  saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());

app.use('/api', s.router);


const PORT = process.env.PORT || 8080;

app.listen(PORT);
